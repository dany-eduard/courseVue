import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

/*  
var componente1 = Vue.extend({
  template: ''
})  

var componente2 = Vue.extend({
  template: '<h3> {{subtitulo}} </h3>',
  data () {
    return {
      subtitulo: 'Listado de JavaScript'
    }
  }
})

var componente3 = Vue.extend({
  template: '<ul><li v-for="fra in frameworks"> {{fra.titulo}} </li></ul>',
  data () {
    return {
      frameworks: [{
        id: 1,
        titulo: 'Vue.js'
      },
      {
        id: 2,
        titulo: 'Angular'
      },
      {
        id: 3,
        titulo: 'React'
      }
      ]
    }
  }
})

Vue.component('comp-saludo', {
  data () {
    return {
      titulo: 'Hola - Introduccion a componentes'
    }
  },
  methods: {
    display () {
      alert('Hello world')
      console.log('Hello world')
    }
  }
})

Vue.component('comp-titulo', componente2)
Vue.component('comp-lista', componente3)
*/
new Vue({
  el: '#app',
  components: {App},
  template: '<App/>'
})
